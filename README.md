# Ansible Role:
An Ansible Role that installs FiveM Server with a configuration Es_Extended/OneSync on RHEL/Fedora, Debian/Ubuntu.
  
## Requirements

None.

## Role Variables

	dsg_user_password: "abc123DEF"
	LATEST_RECOMMENDED_FXSERVER: true
	fivem_dirname: fivem
	fivem_build_id: ~
	fivem_hostname: test fivem dev
	fivem_projectName: ''
	fivem_projectDesc: ''
	fivem_tag: 'DSG'
	fivem_lang: fr-FR
	fivem_steamid_admin: steam:1100001007f6666
	fivem_licenseKey: ajwmeorv9wof08z5ftwxkc6ki1pgqm2k
	fivem_steam_webApi: FDC11AB0A2DF71B8CC8A91EFB5B613
	fivem_max_players: 32
	mariadb_users: 'user'
	mariadb_root_password: 'password'
	mariadb_databases: 'database'
	fivem_port: 30120
	txadmin_port: 40120
  
## Dependencies

	- dsg-role-apache
	- dsg-role-php
	- dsg-role-phpmyadmin
	- dsg-role-mysql
	- dsg-role-firewall
	- dsg-role-fail2ban

## Example Playbook

      ---
	  - hosts: localhost
	    become: yes
        gather_facts: true
     
	    roles:
		 - { role: dsg-role-apache }
		 - { role: dsg-role-php }
		 - { role: dsg-role-phpmyadmin }
		 - { role: dsg-role-mysql }
		 - { role: dsg-role-firewall }
		 - { role: dsg-role-fail2ban }
		 - { role: dsg-role-fivem }

## License

MIT

## Author Information

This role was created in 2021 by Didier MINOTTE
